package priv.lhy.remote;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import priv.lhy.remote.rule.TagRule;

@EnableFeignClients("priv.lhy.remote.feign")
@SpringBootApplication
//@RibbonClient(name = "CLOUD-PAYMENT-SERVICE", configuration = TagRule.class)
public class RemoteServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(RemoteServerApplication.class, args);
    }

}
