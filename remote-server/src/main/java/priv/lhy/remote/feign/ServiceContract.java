package priv.lhy.remote.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;


/**
 * @author :lihy
 * @date :2021/4/25 13:16
 * description :
 **/
@FeignClient(name = "dispense-server")
public interface ServiceContract {

    @GetMapping("/getTag")
    String getTag();
}
