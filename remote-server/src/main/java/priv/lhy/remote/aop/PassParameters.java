package priv.lhy.remote.aop;

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;

/**
 * @author :lihy
 * @date :2021/4/26 10:53
 * description :
 **/
@Slf4j
public class PassParameters {

    private static final ThreadLocal localParameters = new ThreadLocal();

    public static <T> T get() {
        T t = (T) localParameters.get();
        log.info("ThreadID:{}, threadLocal {}", Thread.currentThread().getId(), JSON.toJSONString(t));
        return t;
    }

    public static <T> void set(T t) {
        log.info("ThreadID:{}, threadLocal set {}", Thread.currentThread().getId(), JSON.toJSONString(t));
        localParameters.set(t);
    }
}
