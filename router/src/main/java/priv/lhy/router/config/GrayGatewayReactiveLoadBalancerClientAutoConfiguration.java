package priv.lhy.router.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.cloud.gateway.config.LoadBalancerProperties;
import org.springframework.cloud.loadbalancer.support.LoadBalancerClientFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import priv.lhy.router.filter.TagFilter;

/**
 * @author :lihy
 * @date :2021/4/25 10:34
 * description :
 **/
@Configuration
public class GrayGatewayReactiveLoadBalancerClientAutoConfiguration {

    public GrayGatewayReactiveLoadBalancerClientAutoConfiguration() {
    }



    @Bean
    @ConditionalOnMissingBean({TagFilter.class})
    public TagFilter grayReactiveLoadBalancerClientFilter(LoadBalancerClientFactory clientFactory, LoadBalancerProperties properties) {
        return new TagFilter(clientFactory, properties);
    }
}
