package priv.lhy.remote;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication(scanBasePackages = {"com.legend","priv.lhy"})
@EnableFeignClients
public class RemoteServerUseStaterApplication {

    public static void main(String[] args) {
        SpringApplication.run(RemoteServerUseStaterApplication.class, args);
    }

}
