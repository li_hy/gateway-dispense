package com.legend.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import priv.lhy.remote.feign.ServiceContract;

/**
 * @author :lihy
 * @date :2021/4/25 13:18
 * description :
 **/
@RestController
public class DemoController1 {

    @Autowired
    private ServiceContract serviceContract;

    @GetMapping("/getTag1")
    public String getTag(){
        return serviceContract.getTag();
    }
}
