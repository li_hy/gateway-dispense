package priv.lhy.server.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * @author :lihy
 * @date :2021/4/23 11:18
 * description :
 **/
@Slf4j
@RestController
public class ServerOneController {

    @Value("${spring.cloud.nacos.discovery.metadata.tag}")
    private String tag;

    @GetMapping("/getTag")
    public String getTag(){
        log.info(tag);
        return "tag: " + tag;
    }
}
