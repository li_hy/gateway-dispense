package priv.lhy.aop;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.json.JsonMapper;
import lombok.extern.slf4j.Slf4j;

/**
 * @author :lihy
 * @date :2021/4/26 10:53
 * description :
 **/
@Slf4j
public class PassParameters {

    private static final ThreadLocal localParameters = new ThreadLocal();
    private static JsonMapper mapper = new JsonMapper();

    public static <T> T get() throws JsonProcessingException {
        T t = (T) localParameters.get();

        log.info("ThreadID:{}, threadLocal {}", Thread.currentThread().getId(), mapper.writeValueAsString(t));
        return t;
    }

    public static <T> void set(T t) throws JsonProcessingException {
        log.info("ThreadID:{}, threadLocal set {}", Thread.currentThread().getId(), mapper.writeValueAsString(t));
        localParameters.set(t);
    }
}
