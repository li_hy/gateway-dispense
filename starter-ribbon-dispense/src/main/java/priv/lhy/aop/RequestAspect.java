package priv.lhy.aop;

import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * @author :lihy
 * @date :2021/4/26 10:44
 * description :
 **/
@Aspect
@Slf4j
public class RequestAspect {

    @Pointcut("execution(* priv.lhy..controller..*Controller*.*(..))")
    private void matchMethed(){

    }

    @Before(value = "matchMethed()")
    public void doBefore() throws JsonProcessingException {
        log.info("+++++++++++++++++++++拦截方法获取header++++++++++++++++++++");
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes())
                .getRequest();

        Map<String, String> map = new HashMap<>(1);

        map.put("tag", request.getHeader("tag"));

        PassParameters.set(map);
    }

}
