package priv.lhy.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author :lihy
 * @date :2021/4/26 14:04
 * description :
 **/
@Data
@ConfigurationProperties(prefix = DispenseProperties.PROPERT_PREFIX)
public class DispenseProperties {

    public final static String PROPERT_PREFIX = "com.legend.ribbon.dispense";

    public static String getPropertPrefix() {
        return PROPERT_PREFIX;
    }

    private String key = "tag";

    private Boolean enabled = false;
}
