package priv.lhy.config;

import lombok.AllArgsConstructor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import priv.lhy.aop.RequestAspect;
import priv.lhy.rule.TagRule;

/**
 * @author :lihy
 * @date :2021/4/26 13:57
 * description :
 **/
@Configuration
@ConditionalOnProperty(prefix = "com.legend.ribbon.dispense", name = "enabled", havingValue = "true")
@AllArgsConstructor
@RibbonClient(name = "CLOUD-PAYMENT-SERVICE", configuration = TagRule.class)
public class RuleConfig {

    @Bean
    public TagRule tagRule() {
        return new TagRule();
    }

    @Bean
    public RequestAspect requestAspect() {
        return new RequestAspect();
    }
}
